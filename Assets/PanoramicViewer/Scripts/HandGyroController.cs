﻿using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class HandGyroController : MonoBehaviour
{
    public LeapServiceProvider controller;
    public List<Hand> hands;
    public Vector3 currentDirection;

    public WorldController world;

    public float pitch;
    public float yaw;
    public float roll;

    public float x;
    public float y;

    public AnimationCurve pitchCurve;
    public AnimationCurve pitchCurveCorrection;
    public AnimationCurve rollCurve;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        Frame frame = controller.CurrentFrame; // controller is a Controller object
        hands = frame.Hands;
       
        if (hands.Count > 0)
        {
            var hand = hands[0];
            

            pitch = hand.Direction.Pitch * Mathf.Rad2Deg;
            yaw = hand.Direction.Yaw * Mathf.Rad2Deg;
            roll = hand.PalmNormal.Roll * Mathf.Rad2Deg;




        } else
        {
            pitch = -180;
            yaw = -180;
            roll = 0;
        }

        ConvertToXY();

        world.Up(-y);
        world.Right(-x);
    }

    void ConvertToXY()
    {
        y = pitchCurve.Evaluate(pitch);
        y = pitchCurveCorrection.Evaluate(y);

        x = rollCurve.Evaluate(roll);
        x = pitchCurveCorrection.Evaluate(x);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GyroGuiLogic : MonoBehaviour
{
    // Start is called before the first frame update
    public Image up;
    public Image right;
    public Image down;
    public Image left;


    public HandGyroController controller;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var baseColor = Color.white;
        var minAlpha = 0.0f;



        down.color = new Color(1, 1, 1, controller.y > 0 ? controller.y + minAlpha : minAlpha);
        up.color = new Color(1, 1, 1, controller.y < 0 ? -controller.y + minAlpha : minAlpha);

        left.color = new Color(1, 1, 1, controller.x > 0 ? controller.x + minAlpha : minAlpha);
        right.color = new Color(1, 1, 1, controller.x < 0 ? -controller.x + minAlpha : minAlpha);
    }
}

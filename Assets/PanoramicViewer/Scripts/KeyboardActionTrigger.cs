﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyboardActionTrigger : MonoBehaviour
{

    public List<KeyboardAction> actions;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (actions != null)
        {
            foreach (var a in actions)
            {
                if ((Input.GetKey(a.key) && a.inputType == InputType.Key)
                    || (Input.GetKeyDown(a.key) && a.inputType == InputType.KeyDown)
                    || (Input.GetKeyUp(a.key) && a.inputType == InputType.KeyUp))
                {
                    if (a.action != null)
                    {
                        a.action.Invoke();
                    }
                }
            }
        }
    }


    [System.Serializable]
    public class KeyboardAction
    {
        public KeyCode key;
        public InputType inputType = InputType.KeyDown;
        public UnityEvent action;

    }

    public enum InputType
    {
        Key, KeyDown, KeyUp
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ImageLoader : MonoBehaviour
{
    [SerializeField]
    private List <Texture2D> loadedTextures;

    [SerializeField]
    private WorldController worldController;
    // Start is called before the first frame update
    void Awake()
    {
        loadedTextures = new List<Texture2D>();
        var jpgs = Directory.GetFiles(Application.streamingAssetsPath, "*.jpg", SearchOption.AllDirectories);
        var pngs = Directory.GetFiles(Application.streamingAssetsPath, "*.png", SearchOption.AllDirectories);


        var paths = new List<string>();
        paths.AddRange(jpgs);
        paths.AddRange(pngs);

        foreach (var j in paths)
        {
            print(j);
        }


        foreach (var path in paths)
        {
            var data = File.ReadAllBytes(path);
            Texture2D newTexture = new Texture2D(2, 2);
            newTexture.LoadImage(data);
            loadedTextures.Add(newTexture);
        }

        print("loaded textures " + loadedTextures.Count);
        worldController.texturesArray = loadedTextures.ToArray();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandCanvasController : MonoBehaviour
{
    public LeapServiceProvider controller;
    public List<Hand> hands;

    public CustomGraphicRaycaster raycaster;
    public Camera handCamera;

    [SerializeField]
    private GameObject currentOver;

    public GameObject CurrentOver { 
        get => currentOver; 
        set => currentOver = value; }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        Frame frame = controller.CurrentFrame; // controller is a Controller object
        hands = frame.Hands;
        if (hands.Count > 0)
        {
            var pos = hands[0].PalmPosition;
            var palmPos = new Vector3(pos.x, pos.y, pos.z);
            var screenPos = handCamera.WorldToScreenPoint(palmPos);

            CurrentOver = raycaster.Raycast(screenPos);


        }
    }
}

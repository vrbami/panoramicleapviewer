﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldController : MonoBehaviour
{
    public Transform cam;

    private float yRot;

    private float xRot;

    private float yRotSmoothed;

    private float xRotSmoothed;


    public float maxXRot = 35;

    public Texture2D[] texturesArray;
    private int textureIndex;

    public Renderer r;
    public RawImage rNext;

    public float lerpFactor = 2f;
    public float speed = 0.5f;






    // Start is called before the first frame update
    void Start()
    {
        textureIndex = 0;
        Application.targetFrameRate = 60;
        SetCurrentTexture();

    }

    internal void MoveDirection(int index)
    {
        switch (index)
        {
            case 0:
                Up();
                break;
            case 1:
                Right();
                break;
            case 2:
                Down();
                break;
            case 3:
                Left();
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

        xRotSmoothed = Mathf.Lerp(xRotSmoothed, xRot, Time.deltaTime * lerpFactor);
        yRotSmoothed = Mathf.Lerp(yRotSmoothed, yRot, Time.deltaTime * lerpFactor);
        cam.transform.localRotation = Quaternion.Euler(xRotSmoothed, yRotSmoothed, 0);
    }

    public void Left(float v = 1)
    {
        yRot -= v * speed;
    }

    public void Right(float v = 1)
    {
        yRot += v * speed;
    }

    public void Up(float v = 1)
    {
        xRot -= v * speed;
        ClampValue();
    }

    public void Down(float v = 1)
    {
        xRot += v * speed;
        ClampValue();
    }

    private void ClampValue()
    {
        xRot = Mathf.Clamp(xRot, -maxXRot, maxXRot);
    }

    public void NextTexture()
    {
        textureIndex++;
        textureIndex %= texturesArray.Length;
        SetCurrentTexture();
    }

    private void SetCurrentTexture()
    {
        r.material.mainTexture = texturesArray[textureIndex];
    }


}

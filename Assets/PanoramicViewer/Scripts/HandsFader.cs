﻿using Leap;
using Leap.Unity;
using Leap.Unity.Geometry;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandsFader : MonoBehaviour
{

    public LeapServiceProvider controller;
    public List<Hand> hands;
    public Frustum frustum;

    public float fadeDist = 0.2f;
    private bool fade = true;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Frame frame = controller.CurrentFrame; // controller is a Controller object
        hands = frame.Hands;
        float dist = 0;
        if (hands.Count > 0)
        {
            dist = frustum.Distance(hands[0].PalmPosition.ToVector3());
        }

        var handModels = GetComponentsInChildren<CapsuleHand>();

        float alpha  = 1 - Mathf.Clamp01(dist / fadeDist);
        foreach (var h in handModels)
        {
            h.alpha = alpha;
            

        }


    }

    [Serializable]
    public class Frustum
    {
        public float near = 0.10f;
        public float far = 0.3f;
        public float horizontalAngle = 60f;
        public float verticalAngle = 60f;

        public Vector3 origin = new Vector3(0, -0.3f, 0f);


        public float Distance(Vector3 point)
        {
            Vector3 transformedPoint = point - origin;

            float xDist = Mathf.Abs(transformedPoint.x) - transformedPoint.y * Mathf.Tan(horizontalAngle * 0.5f * Mathf.Deg2Rad);
            xDist = Mathf.Max(xDist, 0);


            float zDist = Mathf.Abs(transformedPoint.z) - transformedPoint.y * Mathf.Tan(verticalAngle * 0.5f * Mathf.Deg2Rad);
            zDist = Mathf.Max(zDist, 0);

            float yDist = 0;
            if (transformedPoint.y < near)
            {
                yDist = near - transformedPoint.y;
            }
            else if (transformedPoint.y > far)
            {
                yDist = transformedPoint.y - far;
            }

            //Debug.Log(xDist + " " +  yDist + " " +  zDist);





            return Mathf.Max(xDist, yDist, zDist);
        }


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HandTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    public UnityEvent stayEvent;

    public bool triggered = false;


    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (triggered)
        {
            stayEvent.Invoke();
        }
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.name == "palm")
        {

            triggered = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "palm")
        {
            triggered = false;
        }
    }




}

﻿

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GuiLogics : MonoBehaviour
{
    public HandCanvasController controller;
    public WorldController worldController;
    public UnityEngine.UI.Image[] arrows;
    public UnityEngine.UI.Image button;
    public TimeButton timeButton;


    private void Awake()
    {
        timeButton.onClicked += NextImage;
    }

    private void NextImage()
    {
        worldController.NextTexture();
    }

    private void Update()
    {
        UnityEngine.UI.Image currenetHovered = null;

        timeButton.hovered = false;
        if (controller.CurrentOver)
        {
            currenetHovered = controller.CurrentOver.GetComponent<UnityEngine.UI.Image>();
            timeButton.hovered = timeButton == controller.CurrentOver.GetComponent<TimeButton>();
        }
        int index = 0;
        foreach (var i in arrows)
        {
            bool isActive = currenetHovered == i;
            i.transform.GetChild(0).gameObject.SetActive(isActive);
            if (isActive)
            {
                worldController.MoveDirection(index);
            }
            index++;
        }






    }







}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeButton : MonoBehaviour
{
    public bool clicked = false;
    public bool hovered = false;
    private float timer = 0;
    public float maxTime = 3f;
    public Image progressImage;

    public delegate void Clicked();
    public event Clicked onClicked;
    public GameObject caption;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!clicked)
        {
            timer += hovered ? Time.deltaTime : -2 * Time.deltaTime;
            timer = Mathf.Clamp(timer, 0, maxTime);


            if (timer >= maxTime)
            {
                clicked = true;
                timer = 0;
                onClicked?.Invoke();
            }
        }
        else
        {
            if (!hovered)
            {
                clicked = false;
            }
        }
        progressImage.fillAmount = timer / maxTime;
        caption.SetActive(hovered && !clicked);
    }
}

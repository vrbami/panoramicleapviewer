﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightLogic : MonoBehaviour
{
    public HandTrigger attachedTrigger;
    private Animator animator;
    private bool isTriggered = false;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (attachedTrigger.triggered && !isTriggered)
        {
            animator.SetTrigger("Highlight");
            isTriggered = true;
            return;
        }

        if (!attachedTrigger.triggered && isTriggered)
        {
            animator.SetTrigger("Normal");
            isTriggered = false;
        }

    }
}

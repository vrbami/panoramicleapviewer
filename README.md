Panorama Leap viewer is a touchless kiosk viewer for 360° panorama photos (and videos in the future releases) created in Unity game engine. 

Our viewer is designed with simplicity in mind - its users (e.g. museum visitors) has usually no experience with touchless/motion control and due to their limited time in the exhibition, they should not have to think too much about how to navigate through the application. The GUI is thus kept minimalistic and we have also not used any hand gestures as our tests showed that they do not feel natural for first-time users (for this specific use-case).

You can see it in action in the following [video](https://vimeo.com/436405713/ba648b50d0).

It was originaly created for National Museum in Czech Republic due to COVID-19 outbreak (and subsequent fear of “hands-on” interactivity in exhibition).

This application is still a work in progress, but can already be easily deployed by building it and copying 360° equirectangular panoramas into StreamingAssets folder  of the build.
